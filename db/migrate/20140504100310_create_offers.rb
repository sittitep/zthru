class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :title
      t.text :short_description
      t.text :description
      t.string :url

      t.timestamps
    end
  end
end
