class AddKeywordsToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :keywords, :text
  end
end
