class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :title
      t.text :content
      t.string :source_name
      t.string :source_url
      t.string :username
      t.integer :offer_id

      t.timestamps
    end
  end
end
