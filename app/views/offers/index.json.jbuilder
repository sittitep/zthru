json.array!(@offers) do |offer|
  json.extract! offer, :id, :title, :short_description, :description, :url
  json.url offer_url(offer, format: :json)
end
