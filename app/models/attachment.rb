class Attachment < ActiveRecord::Base
  belongs_to :offer
  has_attached_file :file
  validates_attachment_content_type :file, :content_type => [/\Aimage\/.*\Z/,"application/pdf"]
end
