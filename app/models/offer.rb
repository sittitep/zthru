class Offer < ActiveRecord::Base
  has_many :reviews
  has_many :attachments

  extend FriendlyId
  friendly_id :title, use: :slugged

  def long_pic
    attachments.find_by_file_type("long")
  end

  def square_pic
    attachments.find_by_file_type("square")
  end

  def review
    attachments.find_by_file_type("review")
  end

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      offer = find_by_id(row["id"]) || new
      offer.attributes = row.to_hash.slice(*offer.attribute_names)
      offer.save!
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
      when ".csv" then Roo::CSV.new(file.path)
      when ".xls" then Excel.new(file.path)
      when ".xlsx" then Excelx.new(file.path)
      else raise "Unknown file type: #{file.original_filename}"
    end
  end
end
