class ReviewsController < ApplicationController
  def import
    Review.import(params[:file])
    redirect_to root_url
  end
end
