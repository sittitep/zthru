class AttachmentsController < ApplicationController
  def create
    @attachment = Attachment.create( attachment_params )
    redirect_to edit_offer_path(@attachment.offer)
  end

  private
    def attachment_params
      params.require(:attachment).permit!
    end
end
