class WelcomeController < ApplicationController
  def index
    @offers = Offer.limit(6)
    @reviews = Review.limit(3)
  end

  def get_in_dept_review
    @offer = Offer.find params[:offer]
  end

  def send_email
    offer = Offer.find params[:offer_id]
    email = Email.find_or_create_by_address(address: params[:email])
    ReviewMailer.send_review(email,offer).deliver
    redirect_to offer_path(offer)
  end
end
