class ReviewMailer < ActionMailer::Base
  default from: "system@zthru.net"

  def send_review(email,offer)
    @offer = offer
    if @offer.review.present?
      attachments["review.pdf"] = File.read(to_utf8(@offer.review.file.path))
    end
    mail(:to => "<#{email.address}>", :subject => "Review on #{offer.title}")
  end

  def to_utf8(str)
    str = str.force_encoding("UTF-8")
    return str if str.valid_encoding?
    str = str.force_encoding("BINARY")
    str.encode("UTF-8", invalid: :replace, undef: :replace)
  end
end
